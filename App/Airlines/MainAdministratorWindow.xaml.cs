﻿using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    /// <summary>
    /// Interaction logic for MainAdministratorWindow.xaml
    /// </summary>
    public partial class MainAdministratorWindow : Window
    {
        Korisnik korisnik;
        public MainAdministratorWindow(Korisnik korisnik)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            Karta.UcitajKarteAministrator();
        }
        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            LetWindow lw = new LetWindow();
            lw.Show();
        }

        private void BtnAerodromi_Click(object sender, RoutedEventArgs e)
        {
            AerodromWindow aw = new AerodromWindow();
            aw.Show();
        }

        private void BtnAviokompanije_Click(object sender, RoutedEventArgs e)
        {
            AviokompanijaWindow akw = new AviokompanijaWindow();
            akw.Show();
        }

        private void BtnKorisnici_Click(object sender, RoutedEventArgs e)
        {
            KorisnikWindow kew = new KorisnikWindow();
            kew.Show();
        }

        private void BtnKarte_Click(object sender, RoutedEventArgs e)
        {
            KartaWindow kartaw = new KartaWindow(korisnik);
            kartaw.Show();
        }

        private void BtnAvioni_Click(object sender, RoutedEventArgs e)
        {
            AvionWindow avionw = new AvionWindow();
            avionw.Show();
        }

        private void BtnSedista_Click(object sender, RoutedEventArgs e)
        {
            SedisteWindow avionw = new SedisteWindow();
            avionw.Show();
        }
    }
}
