﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    /// <summary>
    /// Interaction logic for LetMainWindow.xaml
    /// </summary>
    public partial class LetMainWindow : Window
    {
        ICollectionView view;

        public LetMainWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Letovi);
            DGLetovi.ItemsSource = view;
            DGLetovi.IsSynchronizedWithCurrentItem = true;
        }

        
    }
}
