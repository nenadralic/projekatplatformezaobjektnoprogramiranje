﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    /// <summary>
    /// Interaction logic for KarteUserEditWindow.xaml
    /// </summary>
    public partial class KarteAdministratorEditWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA }

        Karta karta = new Karta();
        Opcija opcija;
        Korisnik korisnik;
        string staraCena;
        int br = 0;
        public KarteAdministratorEditWindow(Karta karta, Korisnik korisnik, Opcija opcija)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.karta = karta;
            this.opcija = opcija;
            this.DataContext = karta;
            foreach (var avi in Data.Instance.Avioni)
            {
                cbSifraLeta.Items.Add(avi.BrLeta);

            }

            cbKlasa.Items.Add("BIZNIS");
            cbKlasa.Items.Add("EKONOMSKA");
            foreach (var kor in Data.Instance.Korisnici)
            {
                cbPutnik.Items.Add(kor.KorIme);

            }

            cbKapija.Items.Add("1");
            cbKapija.Items.Add("2");
            cbKapija.Items.Add("3");
            cbKapija.Items.Add("4");
            cbKapija.Items.Add("5");
            cbKapija.Items.Add("6");
            cbBrReda.Text = karta.BrReda;
            cbSedista.Items.Add("1");
            cbSedista.Items.Add("2");
            cbSedista.Items.Add("3");
            cbSedista.Items.Add("4");
            cbSedista.Items.Add("5");
            cbSedista.Items.Add("6");
            cbSedista.Items.Add("7");
            cbSedista.Items.Add("8");
            cbSedista.Items.Add("9");
            cbSedista.Items.Add("10");

            if (opcija.Equals(Opcija.DODAVANJE))
            {
                cbBrReda.IsEnabled = false;
                cbSedista.IsEnabled = false;
                txtCena.IsEnabled = false;
            }
            if (opcija.Equals(Opcija.IZMENA))
            {
                cbSifraLeta.IsEnabled = false;
                cbKlasa.IsEnabled = false;
                txtCena.IsEnabled = false;
            }
        }



        private bool postojiKarta(string sifra)
        {
            foreach (var karta in Data.Instance.Karte)
            {
                if (karta.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }
        private bool postojiRezervacija(string BrReda, string BrSedista, string klasa, string SifraL)
        {
            foreach (var karta in Data.Instance.Karte)
            {
                if (karta.SifraLeta.Equals(SifraL.Trim()) &&karta.BrReda.Equals(BrReda) && karta.BrSedista.Equals(BrSedista) && karta.KlasaKarte.Equals("BIZNIS"))
                {
                    return true;
                }
                else if (karta.SifraLeta.Equals(SifraL.Trim()) && karta.BrReda.Equals(BrReda) && karta.BrSedista.Equals(BrSedista) && karta.KlasaKarte.Equals("EKONOMSKA"))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(txtSifra.Text) || string.IsNullOrEmpty(cbSifraLeta.Text) || string.IsNullOrEmpty(cbPutnik.Text) || string.IsNullOrEmpty(cbKapija.Text) || string.IsNullOrEmpty(cbKlasa.Text) || string.IsNullOrEmpty(cbBrReda.Text) || string.IsNullOrEmpty(cbSedista.Text) || string.IsNullOrEmpty(txtCena.Text) )
            {
                MessageBox.Show("Sva polja moraju biti popunjena!");
            }
            else
            {
                if (opcija.Equals(Opcija.DODAVANJE))
                {
                    karta.Sifra = txtSifra.Text;
                    karta.SifraLeta = cbSifraLeta.Text;
                    karta.KorIme = cbPutnik.Text;
                    karta.KlasaKarte = cbKlasa.Text;
                    karta.BrReda = cbBrReda.Text;
                    karta.BrSedista = cbSedista.Text;
                    karta.Kapija = cbKapija.Text;
                    karta.CenaKarte = txtCena.Text;
                    karta.Obrisano = true;
                }
                if (opcija.Equals(Opcija.IZMENA))
                {

                    this.DialogResult = true;
                    Karta.IzmeniKartu(karta);
                    br = 1;
                    this.Close();

                }
                else if (opcija.Equals(Opcija.DODAVANJE) && !postojiKarta(karta.Sifra) && !postojiRezervacija(karta.BrReda, karta.BrSedista, karta.KlasaKarte, karta.SifraLeta))
                {

                    this.DialogResult = true;
                    Data.Instance.Karte.Add(karta);
                    Karta.DodajKartu(karta);
                    br = 1;
                    this.Close();
                }
                else if (br == 0 && postojiKarta(karta.Sifra) == true)
                {
                    MessageBox.Show("Postoji karta sa tom sifrom!");
                }
                else if (br == 0 && postojiRezervacija(karta.BrReda, karta.BrSedista, karta.KlasaKarte, karta.SifraLeta) == true)
                {
                    MessageBox.Show("Karta postoji pod tim brojem rezervacije!");
                }
            }
            
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void BtnPotvrdiSifra_Click(object sender, RoutedEventArgs e)
        {
            foreach (var letovi in Data.Instance.Letovi)
            {
                if (cbSifraLeta.SelectedItem.Equals(letovi.Sifra))
                {
                    txtCena.Text = letovi.Cena.ToString();
                    cbSifraLeta.IsEnabled = false;
                }
            }
        }

        private void BtnPonistiSifra_Click(object sender, RoutedEventArgs e)
        {
            cbSifraLeta.IsEnabled = true;
            txtCena.Text = "";
        }



        private void BtnPonistiKlasu_Click(object sender, RoutedEventArgs e)
        {
            cbBrReda.Items.Clear();
            cbSedista.Text = "";
            cbKlasa.SelectedItem = null;
            cbKlasa.IsEnabled = true;
            cbBrReda.IsEnabled = false;
            cbSedista.IsEnabled = false;
            txtCena.Text = staraCena;
        }

        private void BtnPotvrdiKlasa_Click(object sender, RoutedEventArgs e)
        {
            bool br = false;
            foreach (var avion in Data.Instance.Avioni)
            {
                cbBrReda.IsEnabled = true;
                cbSedista.IsEnabled = true;
                if (cbKlasa.SelectedItem.Equals("BIZNIS"))
                {
                    br = false;
                }
                else
                {
                    br = true;
                }
            }
            if (br == false)
            {
                staraCena = txtCena.Text;
                txtCena.Text = (1.5 * float.Parse(txtCena.Text)).ToString();

                cbBrReda.Items.Add("1");
                cbBrReda.Items.Add("2");
                cbKlasa.IsEnabled = false;
            }
            else if (br == true)
            {
                cbBrReda.Items.Add("1");
                cbBrReda.Items.Add("2");
                cbBrReda.Items.Add("3");
                cbBrReda.Items.Add("4");
                cbBrReda.Items.Add("5");
                cbBrReda.Items.Add("6");
                cbKlasa.IsEnabled = false;
            }

        }
       
    }
}
