﻿using Airlines.Database;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Models
{
    public class Aviokompanija : INotifyPropertyChanged, ICloneable
    {
        private string sifra;
        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; }
        }

        private string nazivAviokomanije;
        public string nazivAviokompanije
        {
            get { return nazivAviokomanije; }
            set { nazivAviokomanije = value; }
        }


        private bool obrisano;
        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }


        public object Clone()
        {
            Aviokompanija nova = new Aviokompanija
            {
                Sifra = this.Sifra,
                nazivAviokomanije = this.nazivAviokomanije,
                Obrisano = this.obrisano
            };
            return nova;
        }
        public static void UcitajAviokompanije()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand aviokompanijeCommand = connection.CreateCommand();
                aviokompanijeCommand.CommandText = @"Select distinct * from Aviokompanija";
                SqlDataAdapter daAviokompanije = new SqlDataAdapter();
                daAviokompanije.SelectCommand = aviokompanijeCommand;
                daAviokompanije.Fill(ds, "Aviokompanija");

                foreach (DataRow row in ds.Tables["Aviokompanija"].Rows)
                {
                    Aviokompanija a = new Aviokompanija();
                    a.sifra = (string)row["Sifra"];
                    a.nazivAviokompanije = (string)row["nazivAviokompanije"];
                    a.obrisano = (bool)row["Obrisano"];

                    Data.Instance.Aviokompanije.Add(a);
                }
            }
        }

        public static void DodajAviokompaniju(Aviokompanija a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Aviokompanija values(@sifra,@nazivAviokompanije,1)";

                command.Parameters.Add(new SqlParameter("@Sifra", a.sifra));
                command.Parameters.Add(new SqlParameter("@nazivAviokompanije", a.nazivAviokompanije));
                command.Parameters.Add(new SqlParameter("@Obrisano", a.obrisano));
                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiAviokompaniju(Aviokompanija a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Aviokompanija set obrisano=0 where sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@sifra", a.sifra));

                command.ExecuteNonQuery();

            }
        }

        public static void IzmeniAviokompanije(Aviokompanija a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Aviokompanija SET nazivAviokompanije=@nazivAviokompanije WHERE sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@nazivAviokompanije", a.nazivAviokomanije));
                command.Parameters.Add(new SqlParameter("@sifra", a.sifra));

                command.ExecuteNonQuery();
            }
        }
    }
}
