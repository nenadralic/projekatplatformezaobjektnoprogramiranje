﻿using Airlines.Database;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Models
{
    public class Avion : INotifyPropertyChanged, ICloneable
    {


        private string brLeta;
        public string BrLeta
        {
            get { return brLeta; }
            set { brLeta = value; OnPropertyChanged("BrLeta"); }
        }

        private int brRedovaBiznis;
        public int BrRedovaBiznis
        {
            get { return brRedovaBiznis; }
            set { brRedovaBiznis = value; OnPropertyChanged("BrRedovaBiznis"); }
        }
        private int brRedovaEkonomska;
        public int BrRedovaEkonomska
        {
            get { return brRedovaEkonomska; }
            set { brRedovaEkonomska = value; OnPropertyChanged("BrRedovaEkonomska"); }
        }
        private int brSedistaBiznis;
        public int BrSedistaBiznis
        {
            get { return brSedistaBiznis; }
            set { brSedistaBiznis = value; OnPropertyChanged("BrSedistaBiznis"); }
        }
        private int brSedistaEkonomska;
        public int BrSedistaEkonomska
        {
            get { return brSedistaEkonomska; }
            set { brSedistaEkonomska = value; OnPropertyChanged("brSedistaEkonomska"); }
        }

        private string kompanija;
        public string Kompanija
        {
            get { return kompanija; }
            set { kompanija = value; OnPropertyChanged("Kompanija"); }
        }


        private bool obrisano;
        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        

        public object Clone()
        {
            Avion novi = new Avion
            {

                Kompanija = this.Kompanija,
                BrLeta = this.BrLeta,
                BrRedovaBiznis = this.BrRedovaBiznis,
                BrRedovaEkonomska = this.BrRedovaEkonomska,
                BrSedistaBiznis = this.BrSedistaBiznis,
                BrSedistaEkonomska = this.BrSedistaEkonomska,
                Obrisano = this.Obrisano
                
            };
            return novi;
        }
        public override string ToString()
        {
            return $"BrRedovaBiznis{BrRedovaBiznis} BrRedovaEkonomska{BrRedovaEkonomska} BrSedistaBiznis{BrSedistaBiznis} BrSedistaEkonomska{BrSedistaEkonomska}";
        }
        
        public static void UcitajAvion()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand aviokompanijeCommand = connection.CreateCommand();
                aviokompanijeCommand.CommandText = @"Select distinct * from Avion";
                SqlDataAdapter daAviokompanije = new SqlDataAdapter();
                daAviokompanije.SelectCommand = aviokompanijeCommand;
                daAviokompanije.Fill(ds, "Avion");

                foreach (DataRow row in ds.Tables["Avion"].Rows)
                {
                    Avion a = new Avion();
                    a.BrLeta = (string)row["BrLeta"];
                    a.Kompanija = (string)row["Kompanija"];
                    a.BrRedovaBiznis = (int)row["BrRedovaBiznis"];
                    a.BrRedovaEkonomska = (int)row["BrRedovaEkonomska"];
                    a.BrSedistaBiznis = (int)row["BrSedistaBiznis"];
                    a.BrSedistaEkonomska = (int)row["BrSedistaEkonomska"];
                    a.Obrisano = (bool)row["Obrisano"];
                    Data.Instance.Avioni.Add(a);
                }
            }
        }

        public static void DodajAvion(Avion a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Avion values(@BrLeta,@Kompanija,@BrRedovaBiznis,@BrRedovaEkonomska,@BrSedistaBiznis,@BrSedistaEkonomska,1)";

                command.Parameters.Add(new SqlParameter("@BrLeta", a.BrLeta));
                command.Parameters.Add(new SqlParameter("@Kompanija", a.Kompanija));
                command.Parameters.Add(new SqlParameter("@BrRedovaBiznis", a.BrRedovaBiznis));
                command.Parameters.Add(new SqlParameter("@BrRedovaEkonomska", a.BrRedovaEkonomska));
                command.Parameters.Add(new SqlParameter("@BrSedistaBiznis", a.BrSedistaBiznis));
                command.Parameters.Add(new SqlParameter("@BrSedistaEkonomska", a.BrSedistaEkonomska));
                command.Parameters.Add(new SqlParameter("@Obrisano", a.obrisano));
                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiAvion(Avion a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Avion set obrisano=0 where BrLeta=@BrLeta";

                command.Parameters.Add(new SqlParameter("@BrLeta", a.BrLeta));

                command.ExecuteNonQuery();

            }
        }

        public static void IzmeniAvion(Avion a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Avion SET Kompanija=@Kompanija,BrRedovaBiznis=@BrRedovaBiznis,BrRedovaEkonomska=@BrRedovaEkonomska,BrSedistaBiznis=@BrSedistaBiznis,BrSedistaEkonomska=@BrSedistaEkonomska WHERE BrLeta=@BrLeta";

                command.Parameters.Add(new SqlParameter("@Kompanija", a.Kompanija));
                command.Parameters.Add(new SqlParameter("@BrRedovaBiznis", a.BrRedovaBiznis));
                command.Parameters.Add(new SqlParameter("@BrRedovaEkonomska", a.BrRedovaEkonomska));
                command.Parameters.Add(new SqlParameter("@BrSedistaBiznis", a.BrSedistaBiznis));
                command.Parameters.Add(new SqlParameter("@BrSedistaEkonomska", a.BrSedistaEkonomska));
                command.Parameters.Add(new SqlParameter("@BrLeta", a.BrLeta));

                command.ExecuteNonQuery();
            }
        }
    }
}
