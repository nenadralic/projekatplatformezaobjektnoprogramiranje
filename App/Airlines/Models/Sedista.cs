﻿using Airlines.Database;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Models
{
    public class Sedista : INotifyPropertyChanged, ICloneable
    {
        

        private string sifraLeta;

        public string SifraLeta
        {
            get { return sifraLeta; }
            set { sifraLeta = value; OnPropertyChanged("sifraLeta"); }
        }

        private string brReda;

        public string BrReda
        {
            get { return brReda; }
            set { brReda = value; OnPropertyChanged("brReda"); }
        }
        private string brSedista;

        public string BrSedista
        {
            get { return brSedista; }
            set { brSedista = value; OnPropertyChanged("brSedista"); }
        }
        private string klasa;

        public string Klasa
        {
            get { return klasa; }
            set { klasa = value; OnPropertyChanged("klasa"); }
        }



        private bool obrisano;
        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }



        public object Clone()
        {
            Sedista nova = new Sedista
            {
                SifraLeta = this.SifraLeta,
                BrReda = this.BrReda,
                BrSedista = this.BrSedista,
                Klasa = this.Klasa,
                Obrisano = this.Obrisano
            };
            return nova;
        }

        public static void UcitajSedista()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand aerodromiCommand = connection.CreateCommand();
                aerodromiCommand.CommandText = @"Select  SifraLeta as SifraLeta, BrReda as BrReda, BrSedista as BrSedista, KlasaKarte as Klasa , Obrisano as Obrisano from Karta";
                SqlDataAdapter daAerodromi = new SqlDataAdapter();
                daAerodromi.SelectCommand = aerodromiCommand;
                daAerodromi.Fill(ds, "Sedista");

                foreach (DataRow row in ds.Tables["Sedista"].Rows)
                {
                    Sedista a = new Sedista();
                    a.SifraLeta = (string)row["SifraLeta"];
                    a.BrReda = (string)row["BrReda"];
                    a.BrSedista = (string)row["BrSedista"];
                    a.Klasa = (string)row["Klasa"];
                    a.Obrisano = (bool)row["Obrisano"];

                    Data.Instance.Sediste.Add(a);
                }
            }
        }

         
        

    }
}

