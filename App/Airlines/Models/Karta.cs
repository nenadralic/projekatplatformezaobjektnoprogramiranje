﻿using Airlines.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Models
{
    public class Karta : INotifyPropertyChanged, ICloneable
    {

        private string sifraLeta;
        public string SifraLeta
        {
            get { return sifraLeta; }
            set { sifraLeta = value; OnPropertyChanged("SifraLeta"); }
        }

        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; }
        }
        private string brSedista;
        public string BrSedista
        {
            get { return brSedista; }
            set { brSedista = value; OnPropertyChanged("brSedista"); }
        }

        private string brReda;
        public string BrReda
        {
            get { return brReda; }
            set { brReda = value; OnPropertyChanged("brReda"); }
        }


        private string korIme;
        public string KorIme
        {
            get { return korIme; }
            set { korIme = value; OnPropertyChanged("korIme"); }
        }

        private string kapija;
        public string Kapija
        {
            get { return kapija; }
            set { kapija = value; OnPropertyChanged("Kapija"); }
        }

        private string klasaKarte;
        public string KlasaKarte
        {
            get { return klasaKarte; }
            set { klasaKarte = value; OnPropertyChanged("KlasaKarte"); }
        }



        private string cenaKarte;
        public string CenaKarte
        {
            get { return cenaKarte; }
            set { cenaKarte = value; OnPropertyChanged("CenaKarte"); }
        }

        private bool obrisano;
        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public object Clone()
        {
            Karta nova = new Karta
            {
                SifraLeta = this.SifraLeta,
                BrReda = this.BrReda,
                BrSedista = this.BrSedista,
                KorIme = this.KorIme,
                KlasaKarte = this.KlasaKarte,
                CenaKarte = this.CenaKarte,
                Sifra = this.Sifra,
                Obrisano = this.Obrisano
            };
            return nova;
            
        }
        public override string ToString()
        {
            return $"CenaKarte{CenaKarte} KlasaKarte{KlasaKarte} ";
        }
        public static void UcitajKarteAministrator()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand aviokompanijeCommand = connection.CreateCommand();
                aviokompanijeCommand.CommandText = @"Select distinct * from Karta";
                SqlDataAdapter daAviokompanije = new SqlDataAdapter();
                daAviokompanije.SelectCommand = aviokompanijeCommand;
                daAviokompanije.Fill(ds, "Karta");

                foreach (DataRow row in ds.Tables["Karta"].Rows)
                {
                    Karta a = new Karta();
                    a.Sifra = (string)row["Sifra"];
                    a.SifraLeta = (string)row["SifraLeta"];
                    a.KorIme = (string)row["KorIme"];
                    a.KlasaKarte = (string)row["KlasaKarte"];
                    a.BrReda = (string)row["BrReda"];
                    a.BrSedista = (string)row["BrSedista"];
                    a.Kapija = (string)row["Kapija"];
                    a.CenaKarte = (string)row["CenaKarte"];
                    a.Obrisano = (bool)row["Obrisano"];
                    Data.Instance.Karte.Add(a);
                }
            }
        }
        public static void UcitajKarteUser(Korisnik korisnik)
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet(); 
                
                SqlCommand aviokompanijeCommand = connection.CreateCommand();
                aviokompanijeCommand.Parameters.AddWithValue("@Putnik", korisnik.KorIme);
                aviokompanijeCommand.CommandText = @"Select distinct * from Karta where korIme=@Putnik";
                SqlDataAdapter daAviokompanije = new SqlDataAdapter();
                daAviokompanije.SelectCommand = aviokompanijeCommand;
                daAviokompanije.Fill(ds, "Karta");

                foreach (DataRow row in ds.Tables["Karta"].Rows)
                {
                    Karta a = new Karta();
                    a.Sifra = (string)row["Sifra"];
                    a.SifraLeta = (string)row["SifraLeta"];
                    a.KorIme = (string)row["Putnik"];
                    a.KlasaKarte = (string)row["KlasaKarte"];
                    a.BrReda = (string)row["BrReda"];
                    a.BrSedista = (string)row["BrSedista"];
                    a.Kapija = (string)row["Kapija"];
                    a.CenaKarte = (string)row["CenaKarte"];
                    a.Obrisano = (bool)row["Obrisano"];
                    Data.Instance.Karte.Add(a);
                }
            }
        }

        public static void DodajKartu(Karta a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Karta values(@Sifra,@SifraLeta,@KorIme,@KlasaKarte,@BrReda,@BrSedista,@Kapija,@CenaKarte,1)";

                command.Parameters.Add(new SqlParameter("@Sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@SifraLeta", a.SifraLeta));
                command.Parameters.Add(new SqlParameter("@KorIme", a.KorIme));
                command.Parameters.Add(new SqlParameter("@KlasaKarte", a.KlasaKarte));
                command.Parameters.Add(new SqlParameter("@BrReda", a.BrReda));
                command.Parameters.Add(new SqlParameter("@BrSedista", a.BrSedista));
                command.Parameters.Add(new SqlParameter("@Kapija", a.Kapija));
                command.Parameters.Add(new SqlParameter("@CenaKarte", a.CenaKarte));
                command.Parameters.Add(new SqlParameter("@Obrisano", a.obrisano));
                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiKartu(Karta a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Karta set obrisano=0 where Sifra=@Sifra";

                command.Parameters.Add(new SqlParameter("@Sifra", a.Sifra));

                command.ExecuteNonQuery();

            }
        }

        public static void IzmeniKartu(Karta a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Karta SET SifraLeta=@SifraLeta,KorIme=@KorIme,KlasaKarte=@KlasaKarte,BrReda=@BrReda,BrSedista=@BrSedista,Kapija=@Kapija,CenaKarte=@CenaKarte WHERE Sifra=@Sifra";

                command.Parameters.Add(new SqlParameter("@Sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@SifraLeta", a.SifraLeta));
                command.Parameters.Add(new SqlParameter("@KorIme", a.KorIme));
                command.Parameters.Add(new SqlParameter("@KlasaKarte", a.KlasaKarte));
                command.Parameters.Add(new SqlParameter("@BrReda", a.BrReda));
                command.Parameters.Add(new SqlParameter("@BrSedista", a.BrSedista));
                command.Parameters.Add(new SqlParameter("@Kapija", a.Kapija));
                command.Parameters.Add(new SqlParameter("@CenaKarte", a.CenaKarte));
                command.Parameters.Add(new SqlParameter("@Obrisano", a.obrisano));
                command.ExecuteNonQuery();
            }
        }
    }
}
