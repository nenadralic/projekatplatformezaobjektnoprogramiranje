﻿using Airlines.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Models
{
    
    public class Let : INotifyPropertyChanged, ICloneable
    {
        private string sifra;
        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }
        private string pilot;

        public string Pilot
        {
            get { return pilot; }
            set { pilot = value; OnPropertyChanged("Pilot"); }
        }

        private string odrediste;
        public string Odrediste
        {
            get { return odrediste; }
            set { odrediste = value; OnPropertyChanged("Odrediste"); }
        }

        private string destinacija;
        public string Destinacija
        {
            get { return destinacija; }
            set { destinacija = value; OnPropertyChanged("Destinacija"); }
        }

        private double cena;
        public double Cena
        {
            get { return cena; }
            set { cena = value; OnPropertyChanged("Cena"); }
        }


        private string vremePolaska;
        public string VremePolaska
        {
            get { return vremePolaska; }
            set { vremePolaska = value; OnPropertyChanged("VremePolaska"); }
        }

        private string vremeDolaska;
        public string VremeDolaska
        {
            get { return vremeDolaska; }
            set { vremeDolaska = value; OnPropertyChanged("VremeDolaska"); }
        }

        private bool obrisano;
        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public object Clone()
        {
            return new Let
            {
                Sifra = this.Sifra,
                Pilot = this.Pilot,
                Destinacija = this.Destinacija,
                VremeDolaska = this.VremePolaska,
                VremePolaska = this.VremeDolaska,
                Odrediste = this.Odrediste,
                AvioKompanija = this.avioKompanija,
                Cena = this.Cena
            };
        }
        private string avioKompanija;

        public string AvioKompanija
        {
            get { return avioKompanija; }
            set { avioKompanija = value; OnPropertyChanged("AvioKompanija"); }
        }

        public override string ToString()
        {
            return $"Sifra{Sifra} Odrediste{Odrediste} Cena{Cena}Destinacija{Destinacija} VremePolaska{VremePolaska} VremeDolaska{VremeDolaska} AvioKompanija{AvioKompanija}";
        }
        public static void UcitajLetove()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand letCommand = connection.CreateCommand();
                letCommand.CommandText = @"Select distinct * from let";
                SqlDataAdapter dalet = new SqlDataAdapter();
                dalet.SelectCommand = letCommand;
                dalet.Fill(ds, "let");

                foreach (DataRow row in ds.Tables["let"].Rows)
                {
                    Let l = new Let();
                    l.Sifra = (string)row["Sifra"];
                    l.Pilot = (string)row["Pilot"];
                    l.VremePolaska = (string)row["VremePolaska"];
                    l.VremeDolaska = (string)row["VremeDolaska"];
                    l.Cena = (int)row["Cena"];
                    l.Destinacija = (string)row["Destinacija"];
                    l.Odrediste = (string)row["Odrediste"];
                    l.avioKompanija = (string)row["Aviokompanija"];
                    l.Obrisano = (bool)row["Obrisano"];

                    Data.Instance.Letovi.Add(l);
                }
            }
        }

        public static void DodajLet(Let a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO let values(@Pilot,@Sifra,@VremePolaska,@VremeDolaska,@Cena,@Destinacija,@Odrediste,1,@AvioKompanija)";

                command.Parameters.Add(new SqlParameter("@Pilot", a.Pilot));
                command.Parameters.Add(new SqlParameter("@Sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@VremePolaska", a.VremePolaska));
                command.Parameters.Add(new SqlParameter("@VremeDolaska", a.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@Cena", a.Cena));
                command.Parameters.Add(new SqlParameter("@Destinacija", a.Destinacija));
                command.Parameters.Add(new SqlParameter("@Odrediste", a.Odrediste));
                command.Parameters.Add(new SqlParameter("@Obrisano", a.Obrisano));
                command.Parameters.Add(new SqlParameter("@AvioKompanija", a.AvioKompanija));

                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiLet(Let a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update let set obrisano=0 where sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@sifra", a.Sifra));                command.ExecuteNonQuery();

            }
        }

        public static void IzmeniLet(Let a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE let SET pilot=@pilot,VremePolaska=@VremePolaska,VremeDolaska=@VremeDolaska,cena=@cena,Destinacija=@Destinacija,Odrediste=@Odrediste,aviokompanija=@avioKompanija where sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@pilot", a.pilot));
                command.Parameters.Add(new SqlParameter("@Sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@VremePolaska", a.vremePolaska));
                command.Parameters.Add(new SqlParameter("@VremeDolaska", a.vremeDolaska));
                command.Parameters.Add(new SqlParameter("@cena", a.cena));
                command.Parameters.Add(new SqlParameter("@Destinacija", a.destinacija));
                command.Parameters.Add(new SqlParameter("@Odrediste", a.odrediste));
                command.Parameters.Add(new SqlParameter("@avioKompanija", a.avioKompanija));


                command.ExecuteNonQuery();
            }
        }
    }
    
}
