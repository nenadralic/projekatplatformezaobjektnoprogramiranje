﻿using Airlines.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Models
{
    public class Korisnik : INotifyPropertyChanged, ICloneable
    {

        private string ime;
        public string Ime
        {
            get { return ime; }
            set { ime = value; }
        }

        private string prezime;
        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; }
        }

        private string adresa;
        public string Adresa
        {
            get { return adresa; }
            set { adresa = value; }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private EPol pol;
        public EPol Pol
        {
            get { return pol; }
            set { pol = value; }
        }


        private string korIme;
        public string KorIme
        {
            get { return korIme; }
            set { korIme = value; }
        }

        private string lozinka;
        public string Lozinka
        {
            get { return lozinka; }
            set { lozinka = value; }
        }


        private ETip tip;
        public ETip Tip
        {
            get { return tip; }
            set { tip = value; }
        }


        private bool obrisano;
        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        public object Clone()
        {
            Korisnik novi = new Korisnik
            {
                Ime = this.Ime,
                Prezime = this.Prezime,
                Email = this.Email,
                Adresa = this.Adresa,
                Pol = this.Pol,
                KorIme = this.KorIme,
                Lozinka = this.Lozinka,
                Tip = this.Tip
            };

            return novi;
        }

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"{Ime} {Prezime} {Adresa} {Email} {Pol} {KorIme} {Lozinka} {Tip}";
        }
        public static void UcitajKorisnike()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand korisnikCommand = connection.CreateCommand();
                korisnikCommand.CommandText = @"Select distinct * from Korisnik";
                SqlDataAdapter daKorisnici = new SqlDataAdapter();
                daKorisnici.SelectCommand = korisnikCommand;
                daKorisnici.Fill(ds, "Korisnik");

                foreach (DataRow row in ds.Tables["korisnik"].Rows)
                {
                    Korisnik k = new Korisnik();
                    k.pol = (EPol)row["Pol"];
                    k.ime = (string)row["Ime"];
                    k.prezime = (string)row["Prezime"];
                    k.email = (string)row["Email"];
                    k.adresa = (string)row["Adresa"];
                    k.korIme = (string)row["korIme"];
                    k.lozinka = (string)row["Lozinka"];
                    k.tip = (ETip)row["Tip"];
                    k.obrisano = (bool)row["Obrisano"];

                    Data.Instance.Korisnici.Add(k);
                }
            }
        }

        public static void DodajKorisnika(Korisnik a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO korisnik values(@ime,@prezime,@Adresa,@email,@pol,@korIme,@Lozinka,@Tip,1)";

                command.Parameters.Add(new SqlParameter("@Ime", a.ime));
                command.Parameters.Add(new SqlParameter("@Prezime", a.prezime));
                command.Parameters.Add(new SqlParameter("@Adresa", a.adresa));
                command.Parameters.Add(new SqlParameter("@Email", a.email));
                command.Parameters.Add(new SqlParameter("@Pol", a.pol));
                command.Parameters.Add(new SqlParameter("@korIme", a.korIme));
                command.Parameters.Add(new SqlParameter("@Lozinka", a.lozinka));
                command.Parameters.Add(new SqlParameter("@Tip", a.tip));
                command.Parameters.Add(new SqlParameter("@Obrisano", a.obrisano));

                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiKorisnika(Korisnik a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update korisnik set Obrisano=0 where korIme=@korIme";

                command.Parameters.Add(new SqlParameter("@korisnickoIme", a.korIme));

                command.ExecuteNonQuery();

            }
        }

        public static void IzmeniKorisnika(Korisnik a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE korisnik SET ime=@ime, prezime=@prezime,email=@email,adresa=@adresa,pol=@pol,lozinka=@lozinka,tip=@tip WHERE korIme=@korIme";

                command.Parameters.Add(new SqlParameter("@ime", a.ime));
                command.Parameters.Add(new SqlParameter("@prezime", a.prezime));
                command.Parameters.Add(new SqlParameter("@email", a.email));
                command.Parameters.Add(new SqlParameter("@adresa", a.adresa));
                command.Parameters.Add(new SqlParameter("@pol", a.pol));
                command.Parameters.Add(new SqlParameter("@korisnickoIme", a.korIme));
                command.Parameters.Add(new SqlParameter("@lozinka", a.lozinka));
                command.Parameters.Add(new SqlParameter("@tipKorisnika", a.tip));
                command.Parameters.Add(new SqlParameter("@deleted", a.obrisano));

                command.ExecuteNonQuery();
            }
        }
    }
}
