﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airlines.Database;

namespace Airlines.Models
{
    public class Aerodrom : INotifyPropertyChanged, ICloneable
    {

        public event PropertyChangedEventHandler PropertyChanged;

        
        private string naziv;
        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; OnPropertyChanged("Naziv"); }
        }

        private string sifra;
        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        private string grad;
        public string Grad
        {
            get { return grad; }
            set { grad = value; OnPropertyChanged("Grad"); }
        }

        private bool obrisano;
        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }


        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public object Clone()
        {
            Aerodrom novi = new Aerodrom
            {
                Sifra = this.Sifra,
                Grad = this.Grad,
                Naziv = this.Naziv,
                Obrisano = this.Obrisano
            };

            return novi;
        }

        public override string ToString()
        {
            return $"sifra {Sifra} Naziv {Naziv} Grad {Grad}";
        }
        public static void UcitajAerodrome()
        {
            using (SqlConnection connection = new SqlConnection(Data.CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand aerodromiCommand = connection.CreateCommand();
                aerodromiCommand.CommandText = @"Select distinct * from aerodrom";
                SqlDataAdapter daAerodromi = new SqlDataAdapter();
                daAerodromi.SelectCommand = aerodromiCommand;
                daAerodromi.Fill(ds, "aerodrom");

                foreach (DataRow row in ds.Tables["aerodrom"].Rows)
                {
                    Aerodrom a = new Aerodrom();
                    a.Sifra = (string)row["Sifra"];
                    a.Naziv = (string)row["Naziv"];
                    a.Grad = (string)row["Grad"];
                    a.Obrisano = (bool)row["Obrisano"];

                    Data.Instance.Aerodromi.Add(a);
                }
            }
        }

        public static void DodajAerodrom(Aerodrom a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO aerodrom values(@Sifra,@Naziv,@Grad,1)";

                command.Parameters.Add(new SqlParameter("@Sifra", a.sifra));
                command.Parameters.Add(new SqlParameter("@Naziv", a.naziv));
                command.Parameters.Add(new SqlParameter("@Grad", a.grad));
                command.Parameters.Add(new SqlParameter("@deleted", a.Obrisano));

                command.ExecuteNonQuery();


            }

        }

        public static void ObrisiAerodrom(Aerodrom a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {

                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update aerodrom set Obrisano=0 where sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@sifra", a.sifra));

                command.ExecuteNonQuery();

            }
        }

        public static void IzmeniAerodrom(Aerodrom a)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE aerodrom SET Grad=@Grad, Naziv=@Naziv  WHERE sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@Naziv", a.naziv));
                command.Parameters.Add(new SqlParameter("@Grad", a.grad));
                command.Parameters.Add(new SqlParameter("@Sifra", a.sifra));

                command.ExecuteNonQuery();
            }
        }

    }
}
