﻿using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Airlines
{
    
    public partial class MainWindow : Window
    {
       

        public MainWindow()
        {
            
            InitializeComponent();
            Let.UcitajLetove();
        }

        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            LetMainWindow lw = new LetMainWindow();
            lw.Show();
        }

        private void BtnPrijava_Click(object sender, RoutedEventArgs e)
        {
            
            MainLoginWindow ml = new MainLoginWindow();
            ml.ShowDialog();
            this.Close();

        }


        private void BtnRegistracija_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = new Korisnik();
            korisnik.Tip = ETip.PUTNIK;

            KorisnikEditWindow reg = new KorisnikEditWindow(korisnik, KorisnikEditWindow.Opcija.DODAVANJE);
            reg.cbTip.IsEnabled = false;
            reg.ShowDialog();
        }

        
    }
}
