﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    
    public partial class AerodromWindow : Window
    {
        ICollectionView view;

        public AerodromWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Aerodromi);
            DGAerodromi.ItemsSource = Data.Instance.Aerodromi;
            DGAerodromi.IsSynchronizedWithCurrentItem = true;
        }

   

        private ObservableCollection<Aerodrom> AktivniAerodromi(ObservableCollection<Aerodrom> sviAerodromi)
        {
            ObservableCollection<Aerodrom> aktivniAerodromi = new ObservableCollection<Aerodrom>();
            foreach (Aerodrom aerodrom in sviAerodromi)
            {
                aktivniAerodromi.Add(aerodrom);
            }

            return aktivniAerodromi;
        }


        private bool SelektovanAerodrom(Aerodrom aerodrom)
        {
            if (aerodrom == null)
            {
                MessageBox.Show("Nije selektovan aerodrom");
                return false;
            }
            return true;
        }

        private int IndeksSelektovanogAerodroma(String sifra)
        {
            var indeks = -1;
            for (int i = 0; i < Data.Instance.Aerodromi.Count; i++)
            {
                if (Data.Instance.Aerodromi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AerodromEditWindow eaw = new AerodromEditWindow(new Aerodrom(), AerodromEditWindow.Opcija.DODAVANJE);
            eaw.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
           
            AerodromEditWindow eaw;
            Aerodrom selektovani = view.CurrentItem as Aerodrom;
            if(selektovani.Obrisano.Equals(false))
            {
                MessageBox.Show("Ne mozes da izmenis!");
            }
            else
            {
                if (SelektovanAerodrom(selektovani))
                {
                    Aerodrom oldAirport = (Aerodrom)selektovani.Clone();
                    eaw = new AerodromEditWindow(selektovani, AerodromEditWindow.Opcija.IZMENA);
                    if (eaw.ShowDialog() != true)
                    {
                        int index = IndeksSelektovanogAerodroma(oldAirport.Sifra);
                        Data.Instance.Aerodromi[index] = oldAirport;
                    }
                }
                DGAerodromi.Items.Refresh();
            }
            
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom selektovani = view.CurrentItem as Aerodrom;
            Data.Instance.Aerodromi.Remove(selektovani);
            Aerodrom.ObrisiAerodrom(selektovani);
        }

     
    }
}
