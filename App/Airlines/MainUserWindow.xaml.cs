﻿using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    /// <summary>
    /// Interaction logic for MainUserWindow.xaml
    /// </summary>
    public partial class MainUserWindow : Window
    {
        Korisnik korisnik;
        public MainUserWindow(Korisnik korisnik)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.DataContext = korisnik;
            Karta.UcitajKarteAministrator();

        }
        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            LetMainWindow lw = new LetMainWindow();
            lw.Show();
        }

        private void BtnProfil_Click(object sender, RoutedEventArgs e)
        {
            KorisnikEditWindow reg = new KorisnikEditWindow(korisnik, KorisnikEditWindow.Opcija.IZMENA);
            reg.cbTip.IsEnabled = false;
            reg.txtKorIme.IsEnabled = false;
            reg.ShowDialog();
        }

        private void BtnKarta_Click(object sender, RoutedEventArgs e)
        {
            KartaWindow kartaw = new KartaWindow(korisnik);
            kartaw.Show();
        }
    }
}
