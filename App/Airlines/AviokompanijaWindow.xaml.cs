﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    
    public partial class AviokompanijaWindow : Window
    {
        ICollectionView view;


        public AviokompanijaWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Aviokompanije);
            DGAviokompanije.ItemsSource = view;
            DGAviokompanije.IsSynchronizedWithCurrentItem = true;
        }


      

        private ObservableCollection<Aviokompanija> AktivneAviokompanije(ObservableCollection<Aviokompanija> sveAviokompanije)
        {

            ObservableCollection<Aviokompanija> aktivneAviokompanije = new ObservableCollection<Aviokompanija>();
            foreach (Aviokompanija aviokompanija in sveAviokompanije)
            {
                aktivneAviokompanije.Add(aviokompanija);
            }

            return aktivneAviokompanije;
        }


        private bool SelektovanaAviokompanija(Aviokompanija aviokompanija)
        {
            if (aviokompanija == null)
            {
                MessageBox.Show("Nije selektovana aviokompanija");
                return false;
            }
            return true;
        }

        private int IndeksSelektovaneAviokompanije(String sifra)
        {
            var indeks = -1;
            for (int i = 0; i < Data.Instance.Aviokompanije.Count; i++)
            {
                if (Data.Instance.Aviokompanije[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }


        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija selektovani = view.CurrentItem as Aviokompanija;
            Data.Instance.Aviokompanije.Remove(selektovani);
            Aviokompanija.ObrisiAviokompaniju(selektovani);
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija selektovanaAviokompanija = (Aviokompanija)DGAviokompanije.SelectedItem;
            if (selektovanaAviokompanija.Obrisano.Equals(false))
            {
                MessageBox.Show("Ne mozes da izmenis!");
            }
            else
            {
                if (SelektovanaAviokompanija(selektovanaAviokompanija))
                {
                    Aviokompanija stara = selektovanaAviokompanija.Clone() as Aviokompanija;
                    AviokompanijaEditWindow eaw = new AviokompanijaEditWindow(selektovanaAviokompanija, AviokompanijaEditWindow.Opcija.IZMENA);
                    if (eaw.ShowDialog() != true)
                    {
                        int indeks = IndeksSelektovaneAviokompanije(selektovanaAviokompanija.Sifra);
                        Data.Instance.Aviokompanije[indeks] = stara;
                    }
                }
                DGAviokompanije.Items.Refresh();
            }
            
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AviokompanijaEditWindow eaw = new AviokompanijaEditWindow(new Aviokompanija(), AviokompanijaEditWindow.Opcija.DODAVANJE);
            eaw.ShowDialog();
        }

        
    }
}
