﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace Airlines
{
    
   
    public partial class MainLoginWindow : Window
    {
        int br = 0;

        public MainLoginWindow()
        {
            InitializeComponent();
            Korisnik.UcitajKorisnike();
            Aerodrom.UcitajAerodrome();
            Aviokompanija.UcitajAviokompanije();
            Avion.UcitajAvion();
            Sedista.UcitajSedista();
        }



        private void BtnPrijava_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtKorIme.Text) || string.IsNullOrEmpty(txtLozinka.Text))
            {
                MessageBox.Show("Morate uneti sifru i loznku!");
            }
            else
            {
                String ussername = txtKorIme.Text.Trim();
                String password = txtLozinka.Text.Trim();
                foreach (var korisnik in Data.Instance.Korisnici)
                {
                    if (korisnik.KorIme.Equals(ussername) && korisnik.Lozinka.Trim().Equals(password) && korisnik.Tip.Equals(ETip.PUTNIK))
                    {

                        MainUserWindow mu = new MainUserWindow(korisnik);
                        mu.Show();
                        br = 1;
                        this.Close();

                    }
                    else if (korisnik.KorIme.Trim().Equals(ussername) && korisnik.Lozinka.Trim().Equals(password) && korisnik.Tip.Equals(ETip.ADMINISTRATOR))
                    {

                        MainAdministratorWindow mu = new MainAdministratorWindow(korisnik);
                        mu.Show();
                        br = 1;
                        this.Close();
                    }
                    
                }
            }
            
        }

    }       
}
