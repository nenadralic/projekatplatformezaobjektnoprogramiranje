﻿using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Airlines.Database
{
    class Data
    {
        public const string CONNECTION_STRING = @"Data Source=.;Initial Catalog=Airlines;Integrated Security=True";

        public ObservableCollection<Aerodrom> Aerodromi { get; set; }
        public ObservableCollection<Let> Letovi { get; set; }
        public ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Aviokompanija> Aviokompanije { get; set; }
        public ObservableCollection<Avion> Avioni { get; set; }
        public ObservableCollection<Karta> Karte { get; set; }
        public ObservableCollection<Sedista> Sediste { get; set; }
        public String UlogovanKorisnik { get; set; }
        public List<string> LetoviAviokomp { get; set; }

        private Data()
        {
            UlogovanKorisnik = String.Empty;
            Aerodromi = new ObservableCollection<Aerodrom>();
            Letovi = new ObservableCollection<Let>();
            Korisnici = new ObservableCollection<Korisnik>();
            Aviokompanije = new ObservableCollection<Aviokompanija>();
            Avioni = new ObservableCollection<Avion>();
            Karte = new ObservableCollection<Karta>();
            Sediste = new ObservableCollection<Sedista>();
        }

        private static Data _instance = null;


        public static Data Instance
        {
            get
            {
                if (_instance == null)

                    _instance = new Data();
                return _instance;

            }
        }

        public IEnumerable<object> Let { get; internal set; }
    }
}
