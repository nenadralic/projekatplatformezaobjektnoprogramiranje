﻿using Airlines.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    /// <summary>
    /// Interaction logic for SedisteWindow.xaml
    /// </summary>
    public partial class SedisteWindow : Window
    {
        ICollectionView view;
        public SedisteWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Sediste);
            DGSedista.ItemsSource = view;
            DGSedista.IsSynchronizedWithCurrentItem = true;
        }
    }
}
