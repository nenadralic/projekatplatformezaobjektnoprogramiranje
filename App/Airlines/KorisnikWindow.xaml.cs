﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    
    public partial class KorisnikWindow : Window
    {

        ICollectionView view;
        public KorisnikWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Korisnici);

            DGKorisnici.ItemsSource = Data.Instance.Korisnici;
            DGKorisnici.IsSynchronizedWithCurrentItem = true;
        }

        

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private ObservableCollection<Korisnik> AktivniKorisnici(ObservableCollection<Korisnik> sviKorisnici)
        {

            ObservableCollection<Korisnik> aktivniKorisnici= new ObservableCollection<Korisnik>();
            foreach (Korisnik korisnik in sviKorisnici)
            {
                sviKorisnici.Add(korisnik);
            }

            return aktivniKorisnici;
        }


        private bool SelektovanKorisnik(Korisnik korisnik)
        {
            if (korisnik == null)
            {
                MessageBox.Show("Nije selektovan korisnik.");
                return false;
            }
            return true;
        }

        private int IndeksSelektovanogKorisnika(String korIme)
        {
            var indeks = -1;
            for (int i = 0; i < Data.Instance.Korisnici.Count; i++)
            {
                if (Data.Instance.Korisnici[i].KorIme.Equals(korIme))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {   

            Korisnik selektovanKorisnik = (Korisnik)DGKorisnici.SelectedItem;
            if (selektovanKorisnik.Obrisano.Equals(false))
            {
                MessageBox.Show("Ne mozes da izmenis!");
            }
            else
            {
                if (SelektovanKorisnik(selektovanKorisnik))
                {
                    Korisnik stari = selektovanKorisnik.Clone() as Korisnik;
                    KorisnikEditWindow eaw = new KorisnikEditWindow(selektovanKorisnik, KorisnikEditWindow.Opcija.IZMENA);
                    if (eaw.ShowDialog() != true)
                    {
                        int indeks = IndeksSelektovanogKorisnika(selektovanKorisnik.KorIme);
                        Data.Instance.Korisnici[indeks] = stari;
                    }
                }
                DGKorisnici.Items.Refresh();
            }
            
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Korisnik selektovani = view.CurrentItem as Korisnik;
            Data.Instance.Korisnici.Remove(selektovani);
            Korisnik.ObrisiKorisnika(selektovani);
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            KorisnikEditWindow eaw = new KorisnikEditWindow(new Korisnik(), KorisnikEditWindow.Opcija.DODAVANJE);
            eaw.ShowDialog();
        }

        

        private void DGKorisnici_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
