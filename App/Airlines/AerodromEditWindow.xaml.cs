﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    /// <summary>
    /// Interaction logic for AerodromEditWindow.xaml
    /// </summary>
    public partial class AerodromEditWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA }
        Aerodrom aerodrom;
        Opcija opcija;
        int br = 0;
        public AerodromEditWindow(Aerodrom aerodrom, Opcija opcija)
        {
            InitializeComponent();
            this.aerodrom = aerodrom;
            this.opcija = opcija;

            this.DataContext = aerodrom;

            txtSifra.Text = aerodrom.Sifra;
            txtGrad.Text = aerodrom.Grad;
            txtNaziv.Text = aerodrom.Naziv;

            if (opcija.Equals(Opcija.IZMENA))
            {
                txtSifra.IsEnabled = false;
            }

        }

        private bool postojiAerodrom(string sifra)
        {
            foreach (var aerodrom in Data.Instance.Aerodromi)
            {
                if (aerodrom.Sifra.Equals(sifra.Trim()))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(txtSifra.Text) || string.IsNullOrEmpty(txtNaziv.Text) || string.IsNullOrEmpty(txtGrad.Text))
            {
                MessageBox.Show("Sva polja moraju biti popunjena!");
            }
            else
            {
                if (opcija.Equals(Opcija.IZMENA))
                {
                    this.DialogResult = true;
                    Aerodrom.IzmeniAerodrom(aerodrom);
                    br = 1;
                    this.Close();
                }
                else if (opcija.Equals(Opcija.DODAVANJE) && !postojiAerodrom(aerodrom.Sifra))
                {
                    aerodrom.Sifra = txtSifra.Text;
                    aerodrom.Naziv = txtNaziv.Text;
                    aerodrom.Grad = txtGrad.Text;
                    aerodrom.Obrisano = true;
                    this.DialogResult = true;
                    Data.Instance.Aerodromi.Add(aerodrom);
                    Aerodrom.DodajAerodrom(aerodrom);
                    br = 1;
                    this.Close();
                }
                if (postojiAerodrom(aerodrom.Sifra) == true && br == 0)
                {
                    MessageBox.Show("Aerodrom postoji pod tom sifrom!");
                }
            }
            
        }
            
        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
