﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    
    public partial class LetEditWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA }
        Let let;
        Opcija opcija;
        int br = 0;
        public LetEditWindow(Let let, Opcija opcija)
        {
            InitializeComponent();
            this.let = let;
            this.opcija = opcija;

            this.DataContext = let;

            txtSifra.Text = let.Sifra;
            txtPilot.Text = let.Pilot;
            txtCena.Text = let.Cena.ToString();
            cbDestinacija.SelectedItem = let.Destinacija;
            cbOdrediste.SelectedItem = let.Odrediste;
            dtVremeDolaska.Text = let.VremeDolaska;
            dtVremePolaska.Text = let.VremePolaska;
            cbAviokompanija.Text = let.AvioKompanija;
            foreach (var aerodrom in Data.Instance.Aerodromi)
            {
                cbDestinacija.Items.Add(aerodrom.Sifra);
            }
            foreach (var aerodrom in Data.Instance.Aerodromi)
            {
                cbOdrediste.Items.Add(aerodrom.Sifra);
            }
            foreach(var aviokompanija in Data.Instance.Aviokompanije)
            {
                cbAviokompanija.Items.Add(aviokompanija.nazivAviokompanije);
            }




            if (opcija.Equals(Opcija.IZMENA))
            {
                txtSifra.IsEnabled = false;
            }
        }

        private bool postojiLet(string sifra)
        {
            foreach (var let in Data.Instance.Letovi)
            {
                if (let.Sifra.Equals(sifra.Trim()))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(txtSifra.Text) || string.IsNullOrEmpty(txtPilot.Text) || string.IsNullOrEmpty(dtVremeDolaska.Text) || string.IsNullOrEmpty(dtVremePolaska.Text) || txtCena.Text.Equals("0") || string.IsNullOrEmpty(cbOdrediste.Text) || string.IsNullOrEmpty(cbDestinacija.Text) || string.IsNullOrEmpty(cbAviokompanija.Text))
            {
                MessageBox.Show("Morate uneti sva polja!");
            }
            else
            {
                if (opcija.Equals(Opcija.IZMENA))
                {
                    this.DialogResult = true;
                    Let.IzmeniLet(let);
                    br = 1;
                    this.Close();

                }
                else if (opcija.Equals(Opcija.DODAVANJE) && !postojiLet(let.Sifra))
                {
                    let.Obrisano = true;
                    Data.Instance.Letovi.Add(let);
                    this.DialogResult = true;
                    Let.DodajLet(let);
                    br = 1;
                    this.Close();
                }
                if (postojiLet(let.Sifra) == true && br == 0)
                {
                    MessageBox.Show("Let postoji sa tom sifrom!");
                }
            }
            
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
