﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    /// <summary>
    /// Interaction logic for AvionEditWindow.xaml
    /// </summary>
    public partial class AvionEditWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA }
        Avion avion;
        Opcija opcija;
        int br = 0;

        public AvionEditWindow(Avion avion, Opcija opcija)
        {
            InitializeComponent();
            this.avion = avion;
            this.opcija = opcija;

            this.DataContext = avion;

            cbbrLeta.Text = avion.BrLeta;
            foreach (var let in Data.Instance.Letovi)
            {
                cbbrLeta.Items.Add(let.Sifra);
            }
            cbKompanija.Text = avion.Kompanija;
            foreach (var aviokompanija in Data.Instance.Aviokompanije)
            {
                cbKompanija.Items.Add(aviokompanija.nazivAviokompanije);
            }
            cbBrRedovaBiznis.Text = avion.BrRedovaBiznis.ToString();
            cbBrRedovaBiznis.Items.Add("2");
            cbBrRedovaEkonomska.Text = avion.BrRedovaEkonomska.ToString();
            cbBrRedovaEkonomska.Items.Add("6");
            cbSedistaBiznis.Text = avion.BrSedistaBiznis.ToString();
            cbSedistaBiznis.Items.Add("20");
            cbSedistaEkonomska.Text = avion.BrSedistaEkonomska.ToString();
            cbSedistaEkonomska.Items.Add("60");

            
        }

        public bool postojiAvion(string sifra)
        {
            foreach (var avion in Data.Instance.Avioni)
            {
                if (avion.BrLeta.Equals(sifra.Trim()))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(cbbrLeta.Text) || string.IsNullOrEmpty(cbKompanija.Text) || string.IsNullOrEmpty(cbBrRedovaBiznis.Text) || string.IsNullOrEmpty(cbBrRedovaEkonomska.Text) || string.IsNullOrEmpty(cbSedistaBiznis.Text) || string.IsNullOrEmpty(cbSedistaEkonomska.Text))
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
            }
            else
            {
                if (opcija.Equals(Opcija.IZMENA))
                {
                    this.DialogResult = true;
                    Avion.IzmeniAvion(avion);
                    br = 1;
                    this.Close();

                }
                else if (opcija.Equals(Opcija.DODAVANJE) && !postojiAvion(avion.BrLeta))
                {
                    Data.Instance.Avioni.Add(avion);
                    this.DialogResult = true;
                    Avion.DodajAvion(avion);

                    this.Close();
                }
                if (postojiAvion(avion.BrLeta) == true)
                {
                    MessageBox.Show("Postoji avion na tom letu!");
                }
            }
            

        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

    }       
}
