﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    /// <summary>
    /// Interaction logic for AvionWindow.xaml
    /// </summary>
    public partial class AvionWindow : Window
    {
        ICollectionView view;

        public AvionWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Avioni);
            DGAvioni.ItemsSource = view;
            DGAvioni.IsSynchronizedWithCurrentItem = true;
        }

       

        private ObservableCollection<Avion> AktivniAvioni(ObservableCollection<Avion> sviAvioni)
        {
            ObservableCollection<Avion> aktivniAvioni = new ObservableCollection<Avion>();
            foreach (Avion avion in sviAvioni)
            {
                aktivniAvioni.Add(avion);
            }

            return aktivniAvioni;
        }


        private bool SelektovanAvion(Avion avion)
        {
            if (avion == null)
            {
                MessageBox.Show("Nije selektovan avion.");
                return false;
            }
            return true;
        }

        private int IndeksSelektovanogAviona(String brLeta)
        {
            var indeks = -1;
            for (int i = 0; i < Data.Instance.Avioni.Count; i++)
            {
                if (Data.Instance.Avioni[i].BrLeta.Equals(brLeta))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }


        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AvionEditWindow aew = new AvionEditWindow(new Avion(), AvionEditWindow.Opcija.DODAVANJE);
            aew.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {

            Avion selektovanAvion = (Avion)DGAvioni.SelectedItem;
            if (selektovanAvion.Obrisano.Equals(false))
            {
                MessageBox.Show("Ne mozes da izmenis!");
            }
            else
            {
                if (SelektovanAvion(selektovanAvion))
                {
                    Avion stari = selektovanAvion.Clone() as Avion;
                    AvionEditWindow aew = new AvionEditWindow(selektovanAvion, AvionEditWindow.Opcija.IZMENA);
                    if (aew.ShowDialog() != true)
                    {
                        int indeks = IndeksSelektovanogAviona(selektovanAvion.BrLeta);
                        Data.Instance.Avioni[indeks] = stari;
                    }
                }
                DGAvioni.Items.Refresh();

            }
           
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Avion selektovani = view.CurrentItem as Avion;
            Data.Instance.Avioni.Remove(selektovani);
            Avion.ObrisiAvion(selektovani);
        }
    }
}
