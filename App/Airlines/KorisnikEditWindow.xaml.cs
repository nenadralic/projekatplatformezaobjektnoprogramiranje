﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    
    public partial class KorisnikEditWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA }
        Korisnik korisnik;
        Opcija opcija;
        int br = 0;

        public KorisnikEditWindow(Korisnik korisnik, Opcija opcija)
        {
            InitializeComponent();
            this.opcija = opcija;
            this.korisnik = korisnik;

            this.DataContext = korisnik;

            txtIme.Text = korisnik.Ime;
            txtPrezime.Text = korisnik.Prezime;
            txtEmail.Text = korisnik.Email;
            txtAdresa.Text = korisnik.Adresa;
            txtKorIme.Text = korisnik.KorIme;
            cbPol.ItemsSource = Enum.GetValues(typeof(EPol));
            cbTip.ItemsSource = Enum.GetValues(typeof(ETip));
            txtLozinka.Text = korisnik.Lozinka;
        }

        private bool postojiKorisnik(string korIme)
        {
            foreach (var korisnik in Data.Instance.Korisnici)
            {
                if (korisnik.KorIme.Equals(korIme.Trim()))
                {
                    return true;
                }
            }
            return false;
        }
        private bool proveraEmail()
        {
            if (txtEmail.Text.Contains(".com") && txtEmail.Text.Contains("@"))
            {
                return true;
            }
            return false;
        }
        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(txtIme.Text) || string.IsNullOrEmpty(txtPrezime.Text) || string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrEmpty(txtAdresa.Text) || string.IsNullOrEmpty(cbPol.Text) || string.IsNullOrEmpty(txtKorIme.Text) || string.IsNullOrEmpty(txtLozinka.Text) || string.IsNullOrEmpty(cbTip.Text))
            {
                MessageBox.Show("Morate uneti sva polja!");
            }
            else
            {

                if (opcija.Equals(Opcija.IZMENA))
                {
                    this.DialogResult = true;
                    Korisnik.IzmeniKorisnika(korisnik);
                    br = 1;
                    this.Close();
                }
                else if (opcija.Equals(Opcija.DODAVANJE) && !postojiKorisnik(korisnik.KorIme) && proveraEmail() == true)
                {
                    korisnik.Obrisano = true;
                    Data.Instance.Korisnici.Add(korisnik);
                    this.DialogResult = true;
                    Korisnik.DodajKorisnika(korisnik);
                    br = 1;
                    this.Close();

                }
                else if (postojiKorisnik(korisnik.KorIme) == true && br == 0)
                {
                    MessageBox.Show("Korisnicko ime postoji!");
                }

            }
            
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
