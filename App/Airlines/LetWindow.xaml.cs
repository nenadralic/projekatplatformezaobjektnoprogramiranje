﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    

    public partial class LetWindow : Window
    {
        ICollectionView view;

        public LetWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Letovi);
            DGLetovi.ItemsSource = view;
            DGLetovi.IsSynchronizedWithCurrentItem = true;
        }

        

        
        private ObservableCollection<Let> AktivniLetovi(ObservableCollection<Let> sviLetovi)
        {
            ObservableCollection<Let> aktivniLetovi = new ObservableCollection<Let>();
            foreach (Let let in sviLetovi)
            {
                aktivniLetovi.Add(let);
            }

            return aktivniLetovi;
        }


        private int IndeksSelektovanogLeta(String sifra)
        {
            var indeks = -1;
            for (int i = 0; i < Data.Instance.Letovi.Count; i++)
            {
                if (Data.Instance.Letovi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanLet(Let let)
        {
            if (let == null)
            {
                MessageBox.Show("Nije selektovan let");
                return false;
            }
            return true;
        }


        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            LetEditWindow elw = new LetEditWindow(new Let(), LetEditWindow.Opcija.DODAVANJE);
            elw.ShowDialog();

        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Let selektovaniLet = (Let)DGLetovi.SelectedItem;
            if (selektovaniLet.Obrisano.Equals(false))
            {
                MessageBox.Show("Ne mozes da izmenis!");
            }
            else
            {
                if (SelektovanLet(selektovaniLet))
                {
                    Let stari = selektovaniLet.Clone() as Let;
                    LetEditWindow elw = new LetEditWindow(selektovaniLet, LetEditWindow.Opcija.IZMENA);
                    if (elw.ShowDialog() != true)
                    {
                        int indeks = IndeksSelektovanogLeta(selektovaniLet.Sifra);
                        Data.Instance.Letovi[indeks] = stari;
                    }
                }
                DGLetovi.Items.Refresh();
            }
            

        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Let selektovani = view.CurrentItem as Let;
            Data.Instance.Letovi.Remove(selektovani);
            Let.ObrisiLet(selektovani);
        }

        
    }
}
