﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{
    /// <summary>
    /// Interaction logic for AviokompanijaEditWindow.xaml
    /// </summary>
    public partial class AviokompanijaEditWindow : Window
    {

        public enum Opcija { DODAVANJE, IZMENA }
        Aviokompanija aviokompanija;
        Opcija opcija;
        int br = 0;
        public AviokompanijaEditWindow(Aviokompanija aviokompanija, Opcija opcija)
        {;
            InitializeComponent();
            this.aviokompanija = aviokompanija;
            this.opcija = opcija;

            this.DataContext = aviokompanija;

            txtSifra.Text = aviokompanija.Sifra;
            txtNazivAviokompanije.Text = aviokompanija.nazivAviokompanije;

            if (opcija.Equals(Opcija.IZMENA))
            {
                txtSifra.IsEnabled = false;
            }

        }

        private bool postojiAviokompanija(string sifra)
        {
            foreach (var avioK in Data.Instance.Aviokompanije)
            {
                if (avioK.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }
       

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(txtNazivAviokompanije.Text) || string.IsNullOrEmpty(txtSifra.Text))
            {
                MessageBox.Show("Sva polja moraju biti popunjena!");
            }
            else
            {
                if (opcija.Equals(Opcija.IZMENA))
                {

                    this.DialogResult = true;
                    Aviokompanija.IzmeniAviokompanije(aviokompanija);
                    br = 1;
                    this.Close();
                }

                else if (opcija.Equals(Opcija.DODAVANJE) && !postojiAviokompanija(aviokompanija.Sifra))
                {

                    this.DialogResult = true;
                    aviokompanija.Sifra = txtSifra.Text;
                    aviokompanija.nazivAviokompanije = txtNazivAviokompanije.Text;
                    aviokompanija.Obrisano = true;
                    Data.Instance.Aviokompanije.Add(aviokompanija);
                    Aviokompanija.DodajAviokompaniju(aviokompanija);
                    br = 1;
                    this.Close();
                }
                if (postojiAviokompanija(aviokompanija.Sifra) == true && br == 0)
                {
                    MessageBox.Show("Aerodrom postoji pod tom sifrom!");
                }
            }
            

        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
