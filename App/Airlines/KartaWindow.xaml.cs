﻿using Airlines.Database;
using Airlines.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Airlines
{

    
    public partial class KartaWindow : Window
    {
        ICollectionView view;
        Korisnik korisnik;
        private ObservableCollection<Karta> Karte { get; set; }
        public KartaWindow(Korisnik korisnik)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            Karte = new ObservableCollection<Karta>();
            foreach(Karta k in Data.Instance.Karte)
            {
                if(k.KorIme.Equals(korisnik.KorIme) && korisnik.Tip.Equals(ETip.PUTNIK))
                {
                    Karte.Add(k);
                    DGKarte.ItemsSource = Karte;
                    DGKarte.IsSynchronizedWithCurrentItem = true;
                }
                else if(korisnik.Tip.Equals(ETip.ADMINISTRATOR))
                {
                    Karte.Add(k);
                    DGKarte.ItemsSource = Karte;
                    DGKarte.IsSynchronizedWithCurrentItem = true;
                }
            }


        }

        

        

        private ObservableCollection<Karta> AktivneKarte(ObservableCollection<Karta> sveKarte)
        {
            ObservableCollection<Karta> aktivneKarte = new ObservableCollection<Karta>();
            foreach(Karta karta in sveKarte)
            {
                aktivneKarte.Add(karta);
            }
            return aktivneKarte;
        }

        private bool SelektovanaKarta(Karta karta)
        {
            if(karta == null)
            {
                MessageBox.Show("Nije selektovana karta.");
                return false;
            }
            return true;
        }

        private int IndeksSelektovaneKarte(string sifra)
        {
            var indeks = -1;
            for(int i = 0; i < Data.Instance.Karte.Count; i++)
            {
                if (Data.Instance.Karte[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }
        

  




        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (korisnik.Tip.Equals(ETip.PUTNIK))
            {
                KarteUserEditWindow elw = new KarteUserEditWindow(new Karta(), korisnik, KarteUserEditWindow.Opcija.DODAVANJE);
                elw.ShowDialog();

            }
            else
            {
                KarteAdministratorEditWindow elw = new KarteAdministratorEditWindow(new Karta(), korisnik, KarteAdministratorEditWindow.Opcija.DODAVANJE);
                elw.ShowDialog();
            }
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Karta selektovanaKarta = (Karta)DGKarte.SelectedItem;
            if (selektovanaKarta.Obrisano.Equals(false))
            {
                MessageBox.Show("Ne mozes da izmenis!");
            }
            else
            {
                if (SelektovanaKarta(selektovanaKarta) && korisnik.Tip.Equals(ETip.PUTNIK))
                {
                    if (selektovanaKarta.KorIme.Equals(korisnik.KorIme))
                    {
                        Karta stari = selektovanaKarta.Clone() as Karta;
                        KarteUserEditWindow elw = new KarteUserEditWindow(selektovanaKarta, korisnik, KarteUserEditWindow.Opcija.IZMENA);
                        if (elw.ShowDialog() != true)
                        {
                            int indeks = IndeksSelektovaneKarte(selektovanaKarta.Sifra);
                            Data.Instance.Karte[indeks] = stari;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Mozes da menjas samo svoje karte!");
                    }
                }
                else if (SelektovanaKarta(selektovanaKarta) && korisnik.Tip.Equals(ETip.ADMINISTRATOR))
                {
                    Karta stari = selektovanaKarta.Clone() as Karta;
                    KarteAdministratorEditWindow elw = new KarteAdministratorEditWindow(selektovanaKarta, korisnik, KarteAdministratorEditWindow.Opcija.IZMENA);
                    if (elw.ShowDialog() != true)
                    {
                        int indeks = IndeksSelektovaneKarte(selektovanaKarta.Sifra);
                        Data.Instance.Karte[indeks] = stari;
                    }
                }
                DGKarte.Items.Refresh();
            }
            
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Karta selektovani = view.CurrentItem as Karta;
            Data.Instance.Karte.Remove(selektovani);
            Karta.ObrisiKartu(selektovani);
        }
    }
}
